# Infrastructure as Code Workshop #

## Pre requisites ##
* AWS CLI: [follow this](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
* CDK CLI `npm install -g aws-cdk`

### AWS user setup:
Follow the following page to configure your AWS CLI for the correct user: [AWS CLI configure user](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html)
1. Insert credentials in `~/.aws/credentials` (Linux & Mac) or `%USERPROFILE%\.aws\credentials` (Windows)
2. Configure region to `eu-west-1` in `~/.aws/config (Linux & Mac) or %USERPROFILE%\.aws\config (Windows)

To make use of the named profile use `--profile <profile_name>` flag in every AWSCLI command.
If you want your life to be easier and dont want to add the flag everytime you can set the `AWS_PROFILE` Environment variable via:
For Linux/Macos (will be set during the lifetime of the current command shell):
```shell
export AWS_PROFILE=<profile_name>
```
For Windows (will only be set after restarting the command shell:
```shell
setx AWS_PROFILE <profile_name>
```

## CloudFormation workshop
This is a tutorial in how to create a infrastructure via cloudformation.

WARNING: Due to multiple users using the same AWS instance some parts will be duplicate and will result in errors.
To prevent this every developer should use their own name as prefix or suffix in the stackname when creating a stack.
In addition there will be need of some internal ip-addresses on AWS, so please be careful and use the correct network range assigned to each.
For example if you have the 60-69 network range please use the following format as CIDRBLOCK when creating subnets:
`CidrBlock: 10.192.<range-number from 60 till 69>.0/24`
example: `CidrBlock: 10.192.69.0/24`.


### Commands to use for cloudformation
NOTE: BEFORE DOING THS PLEASE FOLLOW `AWS user setup` FIRST!   

Use these commands fromt the root directory of this project
* command to deploy stack: 
```shell
aws cloudformation create-stack --stack-name <STACK_NAME> --template-body file://./cloudformation/<CORRECT_YAML_NAME>
```
* command to deploy stack with parameters: 
```shell
aws cloudformation create-stack --stack-name <STACK_NAME> --template-body file://./cloudformation/<CORRECT_YAML_NAME> --parameters  ParameterKey=<KEY_NAME>,ParameterValue=<VALUE>
```
* command to delete stack: 
```shell
aws cloudformation delete-stack --stack-name <ECS_CLUSTER_STACK_NAME>
```

If a `create-stack` command fails (when to create the ecs cluster) it is because the command needs to execute some IAM changes which are prohibited by default, add: `--capabilities CAPABILITY_IAM` to the command give explicit permission
* example: `aws cloudformation create-stack --stack-name <STACK_NAME_ECS_CLUSTER> --template-body file://./cloudformation/ecs-cluster.yaml --capabilities CAPABILITY_IAM`

### 1. Setup the VPC
First we gonna create a Virtual Private Cloud with some private and public subnets. 
The VPC will be used for our later services (in this case the ecs cluster).

NOTE: Because everyone is using the same AWS instance we need unique names for everything we create, so please use your own name as prefix/suffix.

#### Step 1 create VPC
We gonna create a VPC with 2 public subnets and 1 private subnet.
Why 2 public and 1 private? because I want it as cheap as possible but for a load balancer you'll need at least 2 public subnets.
The difference between a private and a public subnet is the internet facing gateway a public subnet has and a private subnet doesn't.

The VPC has 3 properties that need to be set:
- CidrBlock: 0.192.<number in your network range>.0/16
- DnsSupport enabled: yes
- DnsHostnames enabled: yes

For the subnets you need to set the following:
- VpcId: Id of the earlier created vpc. (this could be done via [ref function](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-ref.html))
- AvailabilityZone: eu-west-1 # for the public subnets we need them in different az so suffix one with a and one with b)
- CidrBlock: `10.192.<your network range>.0/24` this is a cidr block that will be used PLEASE USE OWN range
- MapPublicIpOnLaunch: Should have a public ip to talk to (true if public subnet)


### step 2 configuring public subnet routing
Now we gonna create some gateways to access the different subnets.
So we create a Internet gateway that faces the internet for the public subnets.

1. So first create a [AWS::EC2::InternetGateway](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-internetgateway.html)
2. Attach the created internet gateway to the earlier created VPC via [AWS::EC2::VPCGatewayAttachment](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-vpc-gateway-attachment.html)
3. Create a public routing table via [AWS::EC2::RouteTable](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-route-table.html)
4. Attach the routing table to the internet Gateway as a DefaultPublicRoute via [AWS::EC2::Route](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-route.html):
   1. You can use DependsOn to wait on VPCGatewayAttachment to be created
   2. Reference the earlier created Public route table (RouteTableId: ) & InternetGateway (GatewayId:)
   3. Use 0.0.0.0/0 as DestinationCidrBlock
5. Create for each public subnet an association to the public route table via [AWS::EC2::SubnetRouteTableAssociation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-subnet-route-table-assoc.html)
6. Create 2 Elastic IP's (EIP) via [AWS::EC2::EIP](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-eip.html)
7. Create 2 NATGateways that and assign the EIP and public subnets[AWS::EC2::NatGateway](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ec2-natgateway.html)
   1. For the EIP use the AllocationId: and get the AllocationId of the specific EIP using the [getAttribute function](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-getatt.html)

### step 3 configuring private subnet routing
And the last step in configuring the resources for the VPC is the private subnet routing.
1. Create another AWS::EC2::RouteTable but now specific for privatesubnet
2. Create an AWS::EC2::Route that uses one of the earlier created NatGateways and created PrivateRouteTable
3. Create an AWS::EC2::SubnetRouteTableAssociation with the PrivateRouteTable and the PrivateSubnet

### step 4 creating some output parameters
For the other Stacks we need to create resources based upon the resources created in this stack (the subnets for instance).
To do this we need to create [Outputs](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/outputs-section-structure.html).
We need the following 4 outputs:
1. VPC id (!Ref <VPC_RESOURCE_NAME>)
2. PublicSubnet1 id (!Ref <PublicSubnet1_RESOURCE_NAME>)
3. PublicSubnet2 id (!Ref <PublicSubnet2_RESOURCE_NAME>)
4. PrivateSubnet id (!Ref <PrivateSubnet_RESOURCE_NAME>)

### step 5 deploy it
Now we can deploy it, use the cli and first make sure you use the correct profile

## 2. ECS Cluster
Now we have a VPC we need to create an Elastic Container Services (ECS cluster).

### step 1 create input parameter for the VPC stack
First add one parameter to know the name of the VPC stack so we can import the previously created outputs.
For this workshop I'd suggest you use the following settings: 
- name of the parameter: VpcStack
- Type: String
- Description: Name of VPC stack to build off of
- Default: <FILL IN STACK NAME OF VPC STACK>

The default is recommended because it enables you to create-stacks without parameters given


### step 2 create ecs isntance
Create an [AWS::ECS::Cluster](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ecs-cluster.html).

### step 3 configure some security access
Now we configure some SecurityGroup and IAM-Role to have access to our cluster via the correct ports.
Also we gonna import our first output.
For the securitygroup we need to allow certain traffic for both Ingress (incoming traffic) as Egress(outgoing traffic).


1. Create a [AWS::EC2::SecurityGroup](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ec2-security-group.html)
   1. Set the `VpcId` towards our earlier created VPC via using [Fn::ImportValue](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-importvalue.html) (You should use [!Sub](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-sub.html) to get the VPC stack name)
   2. Configure SecurityGroupIngress to forward the following ports (80. 8080, 443) with protocol `tcp` and CidrIp `0.0.0.0/0` 
   3. Configure SecurityGroupEgress exactly like Ingress
2. Create a [AWS::IAM::Role](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-iam-role.html) for our later on service to access to certain services of AWS. (Due to my hate for IAM this is a freebie)

```yaml
  DefaultRole:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
                - ecs.amazonaws.com
                - ecs-tasks.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Path: /
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy
        - arn:aws:iam::aws:policy/AmazonECS_FullAccess
```

### step 3 create the application loadbalancer
Now we create a loadbalancer that will use the SecurityGroup and that will loadbalance trafic to both our Public subnets.
In this part you need to import both the ID's of the public subnets.

1. Create an [AWS::ElasticLoadBalancingV2::LoadBalancer]
   1. Because we need it as a application loadbalancer we set the type to `application`
   2. For this entire project weve been using ipv4 as IpAddressType
   3. Since this loadbalancer will connect to the world the scheme should be `internet-facing`
   4. Set as `SecurityGroups` our earlier created security group
   5. For the subnets use [Fn::ImportValue](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-importvalue.html) again to import both id's of the Public Subnets.
   
### step 4 create output for our api backend service
For the creation of our backend api we need to connect to all the resources made here.
So create Outputs that will return the following:
1. The created ECSCluster
2. The SecurityGroup
3. The IAM-Role
4. The Loadbalancer
5. Bonus: the DNSName created by the loadbalancer, this will be the url to which you can send http requests(tip use [getAtt](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/intrinsic-function-reference-getatt.html))

## Setting up the Container in ECS
As last stack we need to configure our service we want to deploy, I've provided a simple docker which contains a simple api to showcase its working.
To setup the service we need to do the following steps:

1. use parameters to get both VPC as the ECS cluster stacknames and for ease the docker registry url that is needed to deploy.
2. Create a [AWS::ElasticLoadBalancingV2::TargetGroup](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-elasticloadbalancingv2-targetgroup.html) that will setup the healthcheck correctly and the correct port (8080)
3. Create a [AWS::ElasticLoadBalancingV2::Listener](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-elasticloadbalancingv2-listener.html) for the loadbalancer that connects the loadbalancer to forward traffic to the created targetgroup
4. Create a [AWS::Logs::LogGroup](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-logs-loggroup.html) to log the application logging (optionally can be removed if you want to be blind on that part)
5. Create a [AWS::ECS::TaskDefinition](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ecs-taskdefinition.html) which describes the container it need to create
6. Create a [AWS::ECS::Service](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-ecs-service.html) this will maintain the running containers/tasks and associated loadbalancers

But since this is mostly application specific I've provided the entire yaml, you only have to change the parameters + imports (Fn::ImportValue). 


```yaml
Parameters:
  VpcStack:
    Type: String
    Description: Name of VPC stack to build off of
    Default: <FILL IN STACK NAME OF VPC STACK>

  EcsClusterStack:
    Type: String
    Description: Name of ECS Cluster stack to build off of
    Default: <FILL IN STACK NAME OF ECS CLUSTER STACK>

  Image:
    Type: String
    Description: URI of image you would like to use
    Default: "docker.io/dithomas/iac-workshop-aws:nodb"

Resources:
  LoadBalancerTargetGroup:
    Type: AWS::ElasticLoadBalancingV2::TargetGroup
    Properties:
      HealthCheckEnabled: true
      HealthCheckPath: /actuator/health
      HealthCheckPort: 8080
      HealthCheckProtocol: HTTP
      Port: 8080
      Protocol: HTTP
      TargetType: ip
      VpcId:
        Fn::ImportValue: !Sub ${VpcStack}-vpc-id #change this

  LoadBalancerListener:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      DefaultActions:
        - Order: 1
          TargetGroupArn: !Ref LoadBalancerTargetGroup
          Type: forward
      LoadBalancerArn:
        Fn::ImportValue: !Sub ${EcsClusterStack}-alb-arn #change this
      Port: 8080
      Protocol: HTTP
    DependsOn:
      - LoadBalancerTargetGroup

  IacWorkshopAwsLogGroup:
    Type: AWS::Logs::LogGroup
    Properties:
      RetentionInDays: 7

  IacWorkshopBackendService:
    Type: AWS::ECS::Service
    Properties:
      Cluster:
        Fn::ImportValue: !Sub ${EcsClusterStack}-ecs-cluster #change this
      DeploymentController:
        Type: ECS
      DesiredCount: 1
      HealthCheckGracePeriodSeconds: 120
      LaunchType: FARGATE
      LoadBalancers:
        -
          ContainerName: iac-workshop-aws
          ContainerPort: 8080
          TargetGroupArn: !Ref LoadBalancerTargetGroup
      NetworkConfiguration:
        AwsvpcConfiguration:
          SecurityGroups:
            -
              Fn::ImportValue: !Sub ${EcsClusterStack}-default-security-group #change this
          Subnets:
            -
              Fn::ImportValue: !Sub ${VpcStack}-private-subnet-id #change this
      TaskDefinition: !Ref IacWorkshopBackendTaskDefinition


  IacWorkshopBackendTaskDefinition:
    Type: AWS::ECS::TaskDefinition
    Properties:
      ContainerDefinitions:
        -
          Name: iac-workshop-aws
          Essential: true
          Image: !Ref Image
          LogConfiguration:
            LogDriver: awslogs
            Options:
              awslogs-group: !Ref IacWorkshopAwsLogGroup
              awslogs-stream-prefix: iac-workshop-container
              awslogs-region: !Ref AWS::Region
          PortMappings:
            -
              ContainerPort: 8080
              HostPort: 8080
              Protocol: tcp
      Cpu: '256'
      ExecutionRoleArn:
        Fn::ImportValue: !Sub ${EcsClusterStack}-default-role #change this
      Memory: '512'
      NetworkMode: awsvpc
      RequiresCompatibilities:
        -  FARGATE
      TaskRoleArn:
        Fn::ImportValue: !Sub ${EcsClusterStack}-default-role #change this
```

## CLEAN EVERYTHING UP:
Please clean all your created stack by deleting them:
```shell
aws cloudformation delete-stack --stack-name <STACK_NAME>
```

# Documentation:
- [AWS Cloudformation official documentation](https://docs.aws.amazon.com/cloudformation/index.html)
- [AWS Template snippets and samples](dhttps://aws.amazon.com/cloudformation/resources/templates/)
- [AWS Resource and property type reference](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html)
- [AWS CDK official documentation](https://docs.aws.amazon.com/cdk/index.html)
- [AWS CDK Constructs library](https://docs.aws.amazon.com/cdk/api/latest/docs/aws-construct-library.html)



### Steps to follow for locally testing cdk ###
* cdk synth
* deploy the cdk bootstrap so cdk can work and know the difference that are deployed: `cdklocal bootstrap`
* deploy your infrastructure: `cdklocal deploy`

* deploy frontend app manually:  
  `awslocal s3api put-object --bucket iac-workshop-frontend --content-type text/html --key index.html --body ./frontend/index.html`
  `awslocal s3 sync ./frontend/iac-workshop-frontend s3://iac-workshop-frontend`

