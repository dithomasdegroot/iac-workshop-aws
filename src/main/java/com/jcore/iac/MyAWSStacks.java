package com.jcore.iac;

import software.amazon.awscdk.core.App;
import software.amazon.awscdk.core.Environment;
import software.amazon.awscdk.core.StackProps;

public class MyAWSStacks {

    public static void main(final String[] args) {
        App app = new App();

        Environment environment = Environment.builder().account("000000000000").region("eu-west-1").build();
        new FrontendBucketStack(app, FrontendBucketStack.class.getSimpleName(), StackProps.builder().env(environment).build());
        new BackendDynamoDBStack(app, BackendDynamoDBStack.class.getSimpleName(), StackProps.builder().env(environment).build());

        app.synth();
    }
}
