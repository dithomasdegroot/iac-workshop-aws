package com.jcore.iac;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.RemovalPolicy;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.s3.Bucket;
import software.amazon.awscdk.services.s3.deployment.BucketDeployment;
import software.amazon.awscdk.services.s3.deployment.Source;

import java.util.List;

public class FrontendBucketStack extends Stack {
    public FrontendBucketStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public FrontendBucketStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);

        // The code that defines your stack goes here
        Bucket frontendBucket = Bucket.Builder.create(this, "iac-workshop-frontend")
                .bucketName("iac-workshop-frontend")
                .websiteIndexDocument("index.html")
                .publicReadAccess(true)
                .removalPolicy(RemovalPolicy.DESTROY)
                .build();


// NOTE: BucketDeployment makes CDK to do something with aws lambda layers which is not supported in free localstack
//        BucketDeployment s3Deployment = BucketDeployment.Builder.create(this, "iac-workshop-frontend-deployment")
//                .sources(List.of(Source.asset("./frontend")))
//                .contentType("text/html")
//                .destinationBucket(frontendBucket)
//                .build();

    }
}
