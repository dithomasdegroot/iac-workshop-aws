package com.jcore.iac;

import software.amazon.awscdk.core.Construct;
import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.services.dynamodb.Attribute;
import software.amazon.awscdk.services.dynamodb.AttributeType;
import software.amazon.awscdk.services.dynamodb.Table;

class BackendDynamoDBStack extends Stack {

    public BackendDynamoDBStack(final Construct scope, final String id) {
        this(scope, id, null);
    }

    public BackendDynamoDBStack(final Construct scope, final String id, final StackProps props) {
        super(scope, id, props);
        Table.Builder.create(this, "iac-workshop-table-posts")
                .tableName("POST")
                .partitionKey(Attribute.builder().name("id").type(AttributeType.STRING).build())
                .build();

    }
}
